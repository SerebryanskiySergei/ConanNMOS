[requires]
 boost/1.66.0@conan/stable
 cpprestsdk/2.10.2@bincrafters/stable

[options]
 boost:shared=False
 cpprestsdk:shared=False 

[generators]
cmake
