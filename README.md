# NMOS C++ Realization with Conan

## Summary

 * [Description](#description)
 * [Usage](#usage)
    * [Requirements](#requirements)
    * [Compiling](#compiling)

*****

## Description



*****

## Usage

### Requirements

This project requires:

 * The [Clang](https://clang.llvm.org) C++ compiler,
 * The [Cmake](https://cmake.org) tools family,
 * The [Conan](https://conan.io) packages manager.
 * The tools and libraries described in the `conanfile.txt` file.

On linux (Ubuntu and debian-flavor distribution), issue the following commands (this requires the installation of the [pip](https://pip.pypa.io/en/stable/) packages manages):

```
./4C_env.sh
```

### Compiling

Create and enter the building directory:

> **warning** if you want to modify the building directory name, make sure to update the `include(build/conanbuildinfo.cmake)` line in the `CMakeLists.txt` file.

```
$ mkdir build && cd build
```

Define the compiler to be used:

```
$ export CC=clang
$ export CXX=clang++
```

Install the project's requirements, by issuing the command (in the `build` directory):

```
$ conan install .. -s compiler=clang -s compiler.version=3.8 -s compiler.libcxx=libstdc++11 -s cppstd=11 —build=missing -e CXX=/usr/bin/clang++ -e CC=/usr/bin/clang
```

Generate build files (`Makefile`, ...):

```
$ cmake .. -DCMAKE_BUILD_TYPE=Release
```

Compile the project:

```
$ cmake --build .
```

Execute the produced artifact:

```
$ ./bin/micro-service
```

Enjoy! :-)
